var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var EventCategorySchema   = new Schema({
  categoryId: String,
  name:String,
  code:String,
  dateCreeated:Number,
  about: String,
  image:String,
  numofevents:Number,
  status:String
});

module.exports = mongoose.model('EventCategoryModel', EventCategorySchema);