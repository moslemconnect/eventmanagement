var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var MailQueueSchema   = new Schema({
  mailId: String,
  email:String,
  emailsubject:String,
  emailRecipient:String,
  eventdate:String,
  eventname: String,
  status:String,
  isActive:Number
});

module.exports = mongoose.model('MailQueue', MailQueueSchema);