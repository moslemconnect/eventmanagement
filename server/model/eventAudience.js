var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var EventAudienceSchema   = new Schema({
  eventId: String,
  audienceId:String,
  isNotified:String,
  audieneLevel:String,
  timeofinterest:Number,
  status:String
});

module.exports = mongoose.model('EventAudienceModel', EventAudienceSchema);