var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var OrgSchema   = new Schema({
  orgId: String,
  keyUserID:String,
  orgName:String,
  phoneNumber:String,
  email:String,
  dateCreated:Number,
  about: String,
  image: String,
  orgType: String,
  orgStatus:String
});

module.exports = mongoose.model('OrgModel', OrgSchema);