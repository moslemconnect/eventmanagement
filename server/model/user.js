var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
  userId: String,
  firstName:String,
  lastName:String,
  phoneNumber:String,
  email: String,
  password:String,
  image: String,
  userStatus:String
});

module.exports = mongoose.model('User', UserSchema);