var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var EventSchema   = new Schema({
  eventId: String,
  eventName:String,
  eventAddress:String,
  phnNum:String,
  email:String,
  eventCategory:String,
  eventDate:String,
  dateCreeated:Number,
  about: String,
  image: [String],
  keyUserID: String,
  status:String
});

module.exports = mongoose.model('EventModel', EventSchema);