var indexRouter         = require('express').Router();
const usersRoute        = require('./userRoute');
const orgRoute        = require('./orgRoute');
const eventRoute        = require('./eventRoute');
const eventaudience        = require('./AudienceRoute');
const eventcategory        = require('./eventcategoryRoute');

/* GET home page. */
indexRouter.get('/', (req, res) => {
  res.status(200).json({ message: 'Welcome to My Moslem connect restful service' });
});

indexRouter.use('/user',usersRoute);
indexRouter.use('/org',orgRoute);
indexRouter.use('/event',eventRoute);
indexRouter.use('/eventaudience',eventaudience);
indexRouter.use('/eventcategory',eventcategory);

module.exports = indexRouter;
