var eventaudience = require('express').Router();
const Audience    = require ('../model/eventAudience');

eventaudience
.get('/',function(req, res) {
    Audience.find(function(err, audience) {
        if (err)
            res.send(err);
        res.json({status:"00",message:"record found",data:audience});
    });
})
.get('/findByEvent/:eventId',function(req, res) {
    console.log("Searching audience with "+req.params.eventId);
    Audience.find({eventId:req.params.eventId},function(err, eventDetail) {
        if (err)
            res.send({status:"96",data:err,message:"no record found"});
        console.log("emi: "+eventDetail);
        res.json({status:"00",message:"record found",data:eventDetail});
    });
})
.post('/add',function(req, res) {
    var audienceDetail = {};
    audienceDetail = req.body;
    console.log("Searching with eventId "+audienceDetail.eventId +"and Audience id "+audienceDetail.audienceId);
    Audience.findOne({eventId:audienceDetail.eventId,audienceId:audienceDetail.audienceId},
        function(err, prevevent) {
        if (err)
            res.json(err);
        console.log("Found: "+prevevent)
        if(prevevent!=""&&prevevent!=null){
            res.json({ status:"96",message: 'interest already logged',data:prevevent});
        }else{
            var audience  = new Audience();

            audience.eventId        = audienceDetail.eventId;
            audience.audienceId	    = audienceDetail.audienceId;
            audience.timeofinterest = audienceDetail.timeofinterest;
            audience.audieneLevel	= audienceDetail.audieneLevel;
            audience.isNotified 	= audienceDetail.isNotified;
            audience.status	        = audienceDetail.status;
            audience.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ status:"00",message: 'Audience Added!',data:audience});
            });
        }
    });

})
.put('/update',function(req, res) {
    var audienceDetail = req.body;
    console.log("Updating for user with id: "+audienceDetail.eventId);
    Audience.findOne({eventId:audienceDetail.eventId,audienceId:audienceDetail.audienceId}, 
        function(err, audience) {
        if (err)
            res.send(err);
        if(audience==""||audience==null){
            res.json({ message: 'No Audience Found for id: '+audienceDetail.eventId});
        }else{

            audience.timeofinterest = audienceDetail.timeofinterest;
            audience.audieneLevel	= audienceDetail.audieneLevel;
            audience.isNotified 	= audienceDetail.isNotified;
            audience.status	        = audienceDetail.status;

            audience.save(function(err) {
                if (err)
                    res.send(err);
                res.json({status:"00", message: 'Audience Updated!' });
            });
        }
    });
})
.get('/delete/all',function(req, res) {
    Audience.remove(function(err, audience) {
        if (err)
            res.send(err);
        res.json({status:"00",message:"deleted ", data:audience});
    });
})  
module.exports = eventaudience;
