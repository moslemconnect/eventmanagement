var eventCatRoute               = require('express').Router();
const EventCategoryModel    = require ('../model/eventcategory');

eventCatRoute
.get('/',function(req, res) {
    EventCategoryModel.find(function(err, events) {
        if (err)
            res.send(err);
        res.json({status:"00",message:"record found",data:events});
    });
})

.post('/add',function(req, res) {
    var eventCatDetails =  req.body;
    var evid = "evcat"+new Date().getTime();
    console.log("Searching with "+evid);
    EventCategoryModel.findOne({code:eventCatDetails.code},function(err, prevevent) {
        if (err)
            res.json(err);
        if(prevevent!=""&&prevevent!=null){
            res.json({ status:"96",message: 'event category already exists',data:prevevent});
        }else{
            var newEventCat  = new EventCategoryModel();
            newEventCat.categoryId  = evid;
            newEventCat.name        = eventCatDetails.name;
            newEventCat.code        = eventCatDetails.code;
            newEventCat.dateCreeated= new Date().getTime();
            newEventCat.numofevents = 0;
            newEventCat.about       = eventCatDetails.about;
            newEventCat.image 	    = eventCatDetails.image;
            
            newEventCat.status	    = eventCatDetails.status;
            newEventCat.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ status:"00",message: 'usEvent Created!',data:newEventCat});
           
            });
        }
    });

})
.put('/update',function(req, res) {
    var eventCatDetails = req.body;
    console.log("Updating for category with id: "+eventCatDetails.categoryId);
    EventCategoryModel.findOne({categoryId:eventCatDetails.categoryId}, function(err, prevevent) {
        if (err)
            res.send(err);
        if(prevevent==""||prevevent==null){
            res.json({ message: 'No EventCategoryModel Found for id: '+eventCatDetails.categoryId});
        }else{
            prevevent.name          = eventCatDetails.name;
            prevevent.code          = eventCatDetails.code;
            prevevent.numofevents   = eventCatDetails.numofevents;
            prevevent.about         = eventCatDetails.about;
            prevevent.image 	    = eventCatDetails.image;
            prevevent.status	    = eventCatDetails.status;
            prevevent.save(function(err) {
                if (err)
                    res.send(err);
                res.json({status:"00", message: 'user updated!' });
            });
        }
    });
})
.get('/delete/all',function(req, res) {
    EventCategoryModel.remove(function(err, events) {
        if (err)
            res.send(err);
        res.json({status:"00",message:"deleted ", data:events});
    });
})  
module.exports = eventCatRoute;
