var orgRoute    = require('express').Router();
const OrgModel  = require ('../model/organization');
const User      = require ('../model/user');

orgRoute
.get('/mosques',function(req, res) {
    OrgModel.find({orgType:"M"},function(err, Org) {
        if (err)
            res.send(err);
        res.json(Org);
    });
})
.get('/groups',function(req, res) {
    OrgModel.find({orgType:"G"},function(err, Org) {
        if (err)
            res.send(err);
        res.json(Org);
    });
})
.get('/:orgId',function(req, res) {
    console.log("Fetch Organization using id: "+req.params.orgId)
    OrgModel.findOne({orgId:req.params.orgId},function(err, Org) {
        if (err)
            res.send({ status:"96",message: err});
        res.json({ status:"00",message: 'Organization Found',data:Org});
    });
})
.get('/findByUserId/:userId',function(req, res) {
    console.log("Fetch Organization using id: "+req.params.userId)
    OrgModel.findOne({keyUserID:req.params.userId},function(err, Org) {
        if (err)
            res.send({ status:"96",message: err});
        res.json({ status:"00",message: 'Organization Found',data:Org});
    });
})
// .post('/add', upload.single("logoimg2"),function(req,res,next){
    .post('/add',function(req,res,next){
    var orgData = new OrgModel();
    var addOrgData = req.body;
    OrgModel.find({phoneNumber:addOrgData.phoneNumber},function(err, orgdetails) {
        if (err)
            res.json(err);
        if(orgdetails!=""){
            res.json({ message: 'Organization Already exists'});
        }else{
            orgData.orgId       = "MS"+new Date().getTime()+"011";;//addOrgData.orgId;
            orgData.orgName     = addOrgData.orgName;
            orgData.phoneNumber = addOrgData.phoneNumber;
            orgData.email       = addOrgData.email;
            orgData.dateCreated = new Date().getTime();
            orgData.about       = addOrgData.about;
            orgData.image	    = addOrgData.image;//'data:'+req.file.mimetype+';base64,'+new Buffer(fs.readFileSync(req.file.path)).toString("base64")
            orgData.orgType 	= addOrgData.orgType;
            orgData.orgStatus	= "Active";
            orgData.save(function(err,OrgSaved) {
                if (err)
                    res.json({status:"96",message: err});
                var usr = new User();
                usr.userId      = "HY"+new Date().getTime()+OrgSaved.orgId.substring(8,12);
                usr.password    = addOrgData.password;
                usr.email       = OrgSaved.email;
                usr.phoneNumber = OrgSaved.phoneNumber;
                usr.firstName   = OrgSaved.orgName;
                usr.lastName    = OrgSaved.orgName+ " Admin";
                usr.image       = "base64";
                usr.userStatus  = "ACTIVE";
                usr.save(function(err) {
                    if (err)
                        res.json({status:"96",message: err});

                    OrgModel.update({orgId:OrgSaved.orgId},{keyUserID:usr.userId},function(err) {
                        if (err)
                            res.json({status:"96",message: err});
                        // fs.unlink(req.file.path);
                        // res.sendFile(redirectVal);
                        res.send({ status:"200",message: 'Organization created!',data:OrgSaved});
                        // res.status(200)
                        //     .contentType("text/plain")
                        //     .end("File uploaded!");
                    }); 
                });
            });
        }
    });
})
.put('/update',function(req, res) {
    var orgUpdateData = req.body;
    console.log("Updating for Organization with id: "+orgUpdateData.orgId);
    OrgModel.findOne({orgId:orgUpdateData.orgId}, function(err, orgdetails) {
        if (err)
            res.send(err);
        if(orgdetails==""||orgdetails==null){
            res.json({ message: 'No Organization Found with id: '+orgUpdateData.userId});
        }else{
            orgdetails.orgName     = orgUpdateData.orgName;
            orgdetails.email       = orgUpdateData.email;
            orgdetails.phonenUmber = orgUpdateData.phonenUmber;
            orgdetails.dateCreated = orgUpdateData.dateCreated;
            orgdetails.about       = orgUpdateData.about;
            orgdetails.image	   = orgUpdateData.image;
            orgdetails.orgType 	   = orgUpdateData.orgType;
            orgdetails.orgStatus   = orgUpdateData.orgStatus;
            orgdetails.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'Organization updated!' });
            });
        }
    });
})
.get('/delete/all',function(req, res) {
    OrgModel.remove(function(err, orgs) {
        if (err)
            res.send(err);

        res.json(orgs);
    });
})  
module.exports = orgRoute;
