var userRoute = require('express').Router();
const User    = require ('../model/user');
const OrgModel    = require ('../model/organization');

userRoute
.get('/',function(req, res) {
    User.find(function(err, users) {
        if (err)
            res.send(err);

        res.json(users);
    });
})
.get('/findbyId/:userId',function(req, res) {
    console.log("Searhing with: "+req.params.userId)
    User.findOne({userId:req.params.userId},function(err, users) {
        if (err)
            res.send(err);
        console.log("Found: "+users)
        res.json(users);
    });
})
.get('/settings/:userId',function(req, res) {
    console.log("Searhing with: "+req.params.userId)
    User.findOne({userId:req.params.userId},function(err, users) {
        if (err)
            res.send(err);
        OrgModel.findOne({keyUserID:req.params.userId},function(err,orgData){
            if(err)
                res.send(err);
            res.json({message:"Usersettings Found",status:"00",header:users,body:orgData});
        })
        
    });
})
.get('/findByEmail/:emailId',function(req, res) {
    console.log("Searhing with: "+req.params.userId)
    User.findOne({email:req.params.emailId},function(err, users) {
        if (err)
            res.send(err);
        console.log("Found: "+users)
        res.json(users);
    });
})
.post('/signin',function(req, res) {
    var signInDet = {};
    signInDet = req.body;
    console.log("Searching with "+signInDet.email+" and "+signInDet.password);
    User.findOne({email:signInDet.email,password:signInDet.password},function(err, usr) {
        if (err)
            res.json(err);
        console.log("User found is: "+usr)
        if(usr!=""&&usr!=null){
            var usrDetail = {email:signInDet.email,userId:usr.userId}//user id ewud be replaced with session id
            res.json({ status:"00",message: 'Valid Credentilas',data:usrDetail});
        }else{
            res.json({ status:"96", message: 'Invalid credentials' });
        }
    });

})
.post('/signup',function(req,res){
    var user = new User();      // create a new instance of the user model
    var signUpDet = {};
    signUpDet = req.body;	
    console.log(req.body);
    User.find({email:signUpDet.email},function(err, users) {
        if (err)
            res.json(err);
        if(users!=""){
            res.json({ message: 'User Already exists'});
        }else{
            user.userId     = signUpDet.userId;
            user.firstName  = signUpDet.firstName;
            user.lastName   = signUpDet.lastName;
            user.phoneNumber= signUpDet.phoneNumber;
            user.email	    = signUpDet.email;
            user.image 	    = signUpDet.image;
            user.userStatus	= signUpDet.userStatus;
            user.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'user created!',data:user});
                // var userSettings = new UserSettings(defSettings);
                // userSettings.uid = user.uid;
                // userSettings.save(function(err) {
                //     if (err)
                //         res.send(err);
                //     res.json({ message: 'user created!',data:user});
                // });
            });
        }
    });
})
.put('/update',function(req, res) {
    var usrUpdateData = req.body;
    console.log("Updating for user with id: "+usrUpdateData.userId);
    User.findOne({userId:usrUpdateData.userId}, function(err, user) {
        if (err)
            res.send(err);
        if(user==""||user==null){
            res.json({ message: 'No User Found for id: '+usrUpdateData.userId});
        }else{
            // console.log("User component is "+JSON.stringify(user))
            user.firstName  = usrUpdateData.firstName;
            user.lastName   = usrUpdateData.lastName;
            user.phoneNumber= usrUpdateData.phoneNumber;
            user.email	    = usrUpdateData.email;
            user.image 	    = usrUpdateData.image;
            user.userStatus	= usrUpdateData.userStatus;
            user.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'user updated!' });
            });
        }
    });
})
.get('/delete/all',function(req, res) {
    User.remove(function(err, users) {
        if (err)
            res.send(err);

        res.json(users);
    });
}) 
module.exports = userRoute;
