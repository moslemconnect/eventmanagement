var userRoute = require('express').Router();
const Event    = require ('../model/event');
const OrgModel  = require ('../model/organization');
const Audience  = require ('../model/eventAudience');
const MailQueue    = require ('../model/mailqueue');

userRoute
.get('/',function(req, res) {
    Event.find(function(err, events) {
        if (err)
            res.send(err);
        res.json({status:"00",message:"record found",data:events});
    });
})
.get('/findByOrgEmail/:emailId',function(req, res) {
    console.log("Searching events with initiator Email"+req.params.emailId);
    OrgModel.findOne({email:req.params.emailId},function(err, org) {
        if (err)
            res.send(err);
        if(org!=""&&org!=null){
            Event.find({keyUserID:org.keyUserID},function(err, events) {
                if (err)
                    res.send(err);
                res.json({status:"00",message:"record found",data:events});
            });
        }else{
            res.json({status:"96",message:"no record found"});
        }
        
    });
})
.get('/findByOrg/:orgId',function(req, res) {
    console.log("Searching events with initiators iD "+req.params.orgId);
    OrgModel.findOne({orgId:req.params.orgId},function(err, org) {
        if (err)
            res.send(err);
        if(org!=""&&org!=null){
            Event.find({keyUserID:org.keyUserID},function(err, events) {
                if (err)
                    res.send(err);
                res.json({status:"00",message:"record found",data:events});
            });
        }else{
            res.json({status:"96",message:"no record found"});
        }
        
    });
})
.get('/findByEvent/:eventId',function(req, res) {
    console.log("Searching events with "+req.params.eventId);
    Event.findOne({eventId:req.params.eventId},function(err, eventDetail) {
        if (err)
            res.send({status:"96",data:err,message:"no record found"});
        Audience.count({eventId:req.params.eventId},function(err,audience){
            if(err)
                res.send({status:"96",data:err,message:"no record found"});
            var respCont = {head:eventDetail,body:audience}
            res.json({status:"00",message:"record found",data:respCont});
        })
        
    });
})


.post('/add',function(req, res) {
    var eventDetails = {};
    eventDetails = req.body;
    // console.log(req.body)
    // console.log(req.file)
    var evid = "ev"+new Date().getTime()+"011";
    console.log("Searching with "+evid);
    Event.findOne({eventId:evid},function(err, prevevent) {
        if (err)
            res.json(err);
        // console.log("Found: "+prevevent)
        if(prevevent!=""&&prevevent!=null){
            res.json({ status:"96",message: 'event already exists',data:prevevent});
        }else{
            var newEvent  = new Event();
            newEvent.eventId        = evid;//eventDetails.eventId;
            newEvent.eventName      = eventDetails.eventName;
            newEvent.eventCategory  = eventDetails.eventCategory;
            newEvent.dateCreeated   = new Date().getTime();
            newEvent.eventDate      = eventDetails.eventDate;
            newEvent.about	        = eventDetails.about;
            newEvent.eventAddress	= eventDetails.eventAddress;
            newEvent.phnNum	        = eventDetails.phnNum;
            newEvent.email	        = eventDetails.email;
            newEvent.image 	        = eventDetails.image;
            newEvent.keyUserID	    = eventDetails.keyUserID;
            newEvent.status	        = eventDetails.status;
            newEvent.save(function(err) {
                if (err)
                    res.send(err);
                var mailQueue = new MailQueue();

                mailQueue.mailId = "MF"+newEvent.eventId
                mailQueue.emailsubject  = "Reminder for: "+newEvent.eventName
                mailQueue.emailRecipient= newEvent.keyUserID
                mailQueue.eventdate     = newEvent.eventDate
                mailQueue.eventname     = newEvent.eventName
                mailQueue.status        = "UNSENT"
                mailQueue.isActive      = 0
                mailQueue.save(function(err,mailq){
                    if(err)
                        console.log(err)
                        res.json({ status:"00",message: 'usEvent Created!',data:newEvent});
                })//event should be detached later
            });
        }
    });

})
.put('/update',function(req, res) {
    var eventDetails = req.body;
    console.log("Updating for user with id: "+eventDetails.eventId);
    Event.findOne({eventId:eventDetails.eventId}, function(err, prevevent) {
        if (err)
            res.send(err);
        if(prevevent==""||prevevent==null){
            res.json({ message: 'No Event Found for id: '+eventDetails.eventId});
        }else{
            prevevent.eventName         = eventDetails.eventName;
            prevevent.eventCategory     = eventDetails.eventCategory;
            prevevent.dateCreeated      = eventDetails.dateCreeated;
            prevevent.eventDate         = eventDetails.eventDate;
            prevevent.eventAddress      = eventDetails.eventAddress;
            prevevent.phnNum	        = eventDetails.phnNum;
            prevevent.email	            = eventDetails.email;
            prevevent.about	            = eventDetails.about;
            prevevent.image 	        = eventDetails.image;
            prevevent.keyUserID	        = eventDetails.keyUserID;
            prevevent.status	        = eventDetails.status;
            prevevent.save(function(err) {
                if (err)
                    res.send(err);
                res.json({status:"00", message: 'user updated!' });
            });
        }
    });
})
.get('/delete/all',function(req, res) {
    Event.remove(function(err, events) {
        if (err)
            res.send(err);
        res.json({status:"00",message:"deleted ", data:events});
    });
})  
module.exports = userRoute;
