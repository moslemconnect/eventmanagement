var express = require('express');
var app = express();
var fs = require("fs");
var bodyParser = require('body-parser');
var mongoose    = require('mongoose');
mongoose.Promise = require('bluebird');
var logger      = require('morgan');
const router 	= require('./server/route');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

const nodemailer 	= require('nodemailer');
const Email 		= require('email-templates');
const MailQueue   	= require ('./server/model/mailqueue');
const User   		= require ('./server/model/user');
var cron 			= require('node-cron');
cron.schedule('5 * * * * *', function(){
	var myDay = new Date();
	var cronTime = myDay.getUTCDay()+'-'+myDay.getUTCMonth()+' '+myDay.getUTCHours()+'Hr '+myDay.getUTCMinutes()+'mins ';
	sendReminder(cronTime);
	console.log('running a task every minute at '+cronTime);
});

app.use(express.static('web/'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(logger('dev'));
// var ur1 = 'mongodb://127.0.0.1:27017/moslemconnect';
var ur1 = 'mongodb://admin1:admin123@ds241570.mlab.com:41570/moslemconndb'
var db;

mongoose.Promise = global.Promise;
mongoose.connect(ur1,function (err, database){
	if (err) {
		console.log(err);
		console.log("Database connection failed "+err);
		process.exit(1);
	}
	db = database;
	console.log("Database connection to "+ur1+" ready");
	 var server = app.listen(process.env.PORT || 8081, function () {
    var host = server.address().address
		var port = server.address().port;
		console.log("Example app listening at http://%s:%s", host, port);
	  });
});

app.get('/', function (req, res) {
	var redirectVal = __dirname + "/charity/" + "index.html";
	console.log("got here now");
	res.sendFile(redirectVal);
});

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

app.use('/api', router);


var sendReminder = function(cronTime){
	console.log("*******************ron Job Begins ****************************"+cronTime)
	MailQueue.findOne({status:"UNSENT",isActive:0},function(err,mailQueue){
		if(err)
			console.log("MailQueue Fetch failed: "+err)
		if(mailQueue!=""&&mailQueue!=null)	
		{	
			console.log("found : "+mailQueue);
			console.log("found again : "+(mailQueue.eventname))
			console.log("cron user fetch using: "+mailQueue.emailRecipient)
			User.findOne({userId:mailQueue.emailRecipient},function(err,userInfo){
				if(err)
					console.log("Error fetching User")
				if(userInfo==null){
					console.log("No record for User attached to Mailqueue")
				}else
				{
					console.log("found user: "+userInfo)
					var usrEmail    	= userInfo.email;
					var mailSubject 	= mailQueue.emailsubject;
					var businessName 	= userInfo.firstName;
					var transporter 	= nodemailer.createTransport({
							service: 'gmail',
							auth: {
								user: 'moslemconnect@gmail.com',
								pass: 'moslem@1234'
							}
					});
				
					const email = new Email({
						views: {
							options: {
								extension: 'ejs' // <---- HERE
							}
						}
					});
					
					email.render('mars/subject', {
							name: businessName
					})
					.then(renderedHTML=>{
						var mailOptions = {
							from: 'digitalfix9@gmail.com',
							to: usrEmail,
							subject: mailSubject,
							replyTo:'digitalfix9@gmail.com',
							html: renderedHTML
						};
						transporter.sendMail(mailOptions, function(error, info){
							if (error) {
								console.log(error);
								// res.json({ message: 'Mail Sending Failed!',error:error});
							} else {
								MailQueue.update({mailId:mailQueue.mailId},{status:"SENT"},function(err){
									if(err)
										console.log("issue")
									console.log('Email sent: ' + info.response);
								})
							}
							});
			
					})
					.catch(console.error);
				}
			});
		}else{
			console.log("No Record Found for Cron to process")
		}
	var myDay = new Date();
	cronTime = myDay.getUTCDay()+'-'+myDay.getUTCMonth()+' '+myDay.getUTCHours()+'Hr  '+myDay.getUTCMinutes()+'mins ';
	console.log("*******************ron Job ends at  ****************************"+cronTime)
	});

	
}





