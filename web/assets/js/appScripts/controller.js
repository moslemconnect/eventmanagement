var app = angular.module('moslemConnect', []);
app.service("apiServices",function($http){
	var groups=[];
	var mosques=[];
	var events=[];
	var service = {
		fetchUser: function(){
			return document.cookie;
		},
		addOrg: function (orgDetails) {
			var resp="null";
			$.ajax({
				type:"POST",
				contentType : "application/json",
				url:"/api/org/add",
				// data: new FormData($('form')[0]),
				data:orgDetails,
				// cache:false,
				// contentType: false,
				// processData: false,
				contentType : "application/json",
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		login: function (postdata) {
			var resp="null";
			$.ajax({
				type:"POST",
				contentType : "application/json",
				url:"/api/user/signin",
				data: JSON.stringify(postdata),
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchGroups: function () {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/org/groups",
				dataType : 'json',
				async:false,
				success:function (data) {
					// console.log("sUCCESS ==>",data);
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchMosques: function () {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/org/mosques",
				dataType : 'json',
				async:false,
				success:function (data) {
					// console.log("sUCCESS ==>",data);
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchSingleOrg: function (orgId) {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/org/"+orgId,
				dataType : 'json',
				async:false,
				success:function (data) {
					// console.log("sUCCESS ==>",data);
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		updateOrgProfile: function (postdata) {
			var resp="null";
			$.ajax({
				type:"PUT",
				contentType : "application/json",
				url:"/api/org/update",
				data: JSON.stringify(postdata),
				dataType : 'json',
				async:false,
				success:function (data) {
					// console.log("SUCCESS ==>",data);
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchOrgProfile: function (userId) {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/org/findByUserId/"+userId,
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		joinEvent: function (postdata) {
			var resp="null";
			$.ajax({
				type:"POST",
				contentType : "application/json",
				url:"/api/eventaudience/add",
				data: JSON.stringify(postdata),
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		// addEvent: function () {
		// 	var resp="null";
		// 	$.ajax({
		// 		type:"POST",
		// 		contentType : "application/json",
		// 		url:"/api/event/add",
		// 		data: new FormData($('form')[0]),
		// 		cache:false,
		// 		contentType: false,
        //   		processData: false,
		// 		dataType : 'json',
		// 		async:false,
		// 		success:function (data) {
		// 			resp = data;
		// 		},
		// 		error:function (data) {
		// 			console.log("Error ==>",data);
		// 			resp = data;
		// 		}
		// 	});
		// 	return resp;
		// },
		addEvent: function (eventdata) {
			var resp="null";
			$.ajax({
				type:"POST",
				contentType : "application/json",
				url:"/api/event/add",
				data: eventdata,
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		updateEvent: function (eventdata) {
			var resp="null";
			$.ajax({
				type:"PUT",
				contentType : "application/json",
				url:"/api/event/update",
				data: eventdata,
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchOrgEvents: function (userEmail) {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/event/findByOrgEmail/"+userEmail,
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchOrgEventsByID: function (orgId) {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/event/findByOrg/"+orgId,
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchAllEvenets: function () {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/event/",
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchEventById: function (eventId) {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/event/findByEvent/"+eventId,
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchUserSettings: function (userId) {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/user/settings/"+userId,
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		},
		fetchCategories: function () {
			var resp="null";
			$.ajax({
				type:"GET",
				url:"/api/eventcategory",
				dataType : 'json',
				async:false,
				success:function (data) {
					resp = data;
				},
				error:function (data) {
					console.log("Error ==>",data);
					resp = data;
				}
			});
			return resp;
		}
	}
	return service;	
});


app.controller('createOrg', function ($scope,apiServices) {
	var init = {orgName:"",email:"",phonenUmber:"",password:"",password1:"",about:"",orgType:"",orgStatus:"",image:""};
	$scope.orgData 	= init;
	$scope.loggedinUser = getCookie("loggedInUserId");
	$scope.userImg={"fileVal":""};
	$scope.saveOrg 	= function(){
		var reqData 		= $scope.orgData;
		if(reqData.password!=reqData.passwordmatch||reqData.password==""){
			alert("Password does not match or is invalid");
			return;
		}
		
		reqData.orgId 		= "MS"+new Date().getTime();
		console.log("Sending :"+JSON.stringify(reqData));
		var respData 		= apiServices.addOrg(JSON.stringify(reqData));
		alert(respData.message);
		if(respData.status=="00"){
			$scope.orgData = null;
			navToIndex();
		}
	 }

	 $scope.logout = function(){
		 signOut();
	 }
});

app.controller('indexedCntrl', function ($scope,apiServices) {
	// alert("welcome");
	var init = {email:"moslemco@gmail.com",password:"admin@123"};	
	$scope.orgData 	= init;
	$scope.loggedinUser = {userId:""};
	$scope.eventCategories = [];
	$scope.signin 	= function(){
		if($scope.loggedinUser.userId!=""){
			alert("Already logged in");
			return;
		}
		var reqData 		= $scope.orgData;
		var respData 		= apiServices.login(reqData);
		console.log("eni ni: "+JSON.stringify(respData))
		// alert(respData.message)
		if(respData.status=="00"){
			document.cookie = "loggedInUserId="+respData.data.userId;
			alert("Welcome "+respData.data.email)
			$scope.orgData = null;
			navToIndex();
		}else{
			alert(respData.message)
		}
	}

	
	 $scope.attachedOrgDetails = {};

	 $scope.initUser = function(){
		if(getCookie("loggedInUserId")!=""){
			var respData 		= apiServices.fetchUserSettings(getCookie("loggedInUserId"));
			$scope.loggedinUser = respData.status=="00"?respData.header:{userId:""};
			$scope.attachedOrgDetails = respData.body;
			console.log("i have it: "+JSON.stringify($scope.attachedOrgDetails))
		}	
	 }

	 $scope.fetchEventCategories = function(){
		var respData 		= apiServices.fetchCategories();
		if(respData.status=="00"){
			$scope.eventCategories = respData.data;
		}else{
			console.log(JSON.stringify(respData));
		}		
	 }

	 $scope.logout = function(){
		signOut();
	 }

	 var execf1 = $scope.initUser();
	 var execf2 = $scope.fetchEventCategories();
		
});

app.controller('groupCntrl', function ($scope,apiServices) {
	 $scope.groupList = [];
	 $scope.loggedinUser = {userId:""};
	 

	 $scope.initUser = function(){
		if(getCookie("loggedInUserId")!=""){
			var respData 		= apiServices.fetchUserSettings(getCookie("loggedInUserId"));
			$scope.loggedinUser = respData.status=="00"?respData.header:{userId:""};
		}	
	 }

	 $scope.fetchOrg 	= function(){
		var respData 		= apiServices.fetchGroups();
		$scope.groupList = respData;
	 }

	 $scope.logout = function(){
		signOut();
	 }

	 var execf1 = $scope.initUser();
	 var execf2 = $scope.fetchOrg();
});

app.controller('mosqueCntrl', function ($scope,apiServices) {
	 $scope.mosqueList = [];
	 $scope.loggedinUser = {userId:""};
 

	 $scope.initUser = function(){
		if(getCookie("loggedInUserId")!=""){
			var respData 		= apiServices.fetchUserSettings(getCookie("loggedInUserId"));
			// $scope.loggedinUser = respData.status=="00"?respData.data:{userId:""};
			$scope.loggedinUser = respData.status=="00"?respData.header:{userId:""};
		}	
	 }

	 $scope.fetchOrg 	= function(){
		var respData 		= apiServices.fetchMosques();
		$scope.mosqueList = respData;
		console.log(JSON.stringify($scope.mosqueList));
	 }

	 $scope.logout = function(){
		signOut();
	 }

	 var execf1 = $scope.initUser();
	 var execf2 = $scope.fetchOrg();
	 
});

app.controller('eventsCntrl', function ($scope,apiServices) {
	$scope.eventList = [];
	$scope.loggedinUser = {userId:""};
	$scope.searchCriteria = {"searchKey":"","location":"lagos"}

	 $scope.initUser = function(){
		if(getCookie("loggedInUserId")!=""){
			var respData 		= apiServices.fetchUserSettings(getCookie("loggedInUserId"));
			$scope.loggedinUser = respData.status=="00"?respData.header:{userId:""};
		}	
	 }

	 $scope.fetchOrg 	= function(){
		var respData 		= apiServices.fetchAllEvenets();
		$scope.eventList = respData.data;
	 }

	 $scope.logout = function(){
		signOut();
	 }

	 var execf1 = $scope.initUser();
	 var execf2 = $scope.fetchOrg();
});

app.controller('advertCntrl', function ($scope,apiServices) {
	$scope.eventList = [];
	$scope.loggedinUser = {userId:""};
 

	 $scope.initUser = function(){
		if(getCookie("loggedInUserId")!=""){
			var respData 		= apiServices.fetchUserSettings(getCookie("loggedInUserId"));
			$scope.loggedinUser = respData.status=="00"?respData.header:{userId:""};
		}	
	 }

	 $scope.fetchOrg 	= function(){
		var respData 		= apiServices.fetchAllEvenets();
		$scope.eventList = respData.data;
		console.log(JSON.stringify($scope.eventList));
	 }

	 $scope.logout = function(){
		signOut();
	 }

	 var execf1 = $scope.initUser();
	 var execf2 = $scope.fetchOrg();
});

app.controller('eventCntrl', function ($scope,apiServices) {
	$scope.eventContents = {};
	$scope.loggedinUser = {userId:''};
	$scope.eventCount=0;
	$scope.editable=false;
	$scope.joinEventData={ticktype:"",reminderTime:"",reminderTimer:""};
	var eventId;

	$scope.initUser = function(){
		if(getCookie("loggedInUserId")!=""){
			var respData 		= apiServices.fetchUserSettings(getCookie("loggedInUserId"));
			$scope.loggedinUser = respData.status=="00"?respData.header:{userId:""};
		}	
	 }
	 
	$scope.fetchEventContents = function(){
		eventId = window.location.search.split("?")[1];
		// alert(eventId);
		var resp 		= apiServices.fetchEventById(eventId);
		
		if(resp.status=="00"){
			$scope.eventContents = resp.data.head;
			$scope.eventCount = resp.data.body;
			$scope.editable = $scope.eventContents.keyUserID==$scope.loggedinUser.userId?true:false;
			console.log("target: "+JSON.stringify($scope.editable));

		}else{
			console.log(JSON.stringify(resp));
		}	
	 }

	 $scope.joinEvent = function(){
		//  alert("joining soon "+$scope.loggedinUser.userId)
		var tett;
		if($scope.loggedinUser.userId==""){
			document.getElementById("signInTrigger").click();
		}else{
			document.getElementById("joinEventTrigger").click();
			if($scope.joinEventData.ticktype==""){
				return;
			}			
			var audienceDetail = {eventId:eventId,
				audienceId:$scope.loggedinUser.userId,
				timeofinterest:new Date().getTime(),
				audieneLevel:$scope.joinEventData.ticktype,
				isNotified:"true",
				status:"active"}
			var resp 		= apiServices.joinEvent(audienceDetail);
			if(resp.status=="00"){
				$scope.eventCount = $scope.eventCount +1;
			}else{
				alert(resp.message)
			}
			$scope.joinEventData.ticktype="";
		}
	 }

	 $scope.logout = function(){
		signOut();
	 }
	 var execf1 = $scope.initUser();
	 var execf2 = $scope.fetchEventContents();
});

app.controller('adminCntrl', function ($scope,apiServices) {
	
	$scope.orgProfile 	= {orgName:"",email:"",phonenUmber:"",about:"",orgType:"",orgStatus:"",image:""};
	$scope.eventData = {eventName:"",image:null,eventDate:null,eventCategory:"",eventAddress:"",email:"",phnNum:"",about:"",btn:"Save",eventOps:"Add Event"};
	$scope.orgContents = {orgTitle:"",conts:[]};
	$scope.loggedinUser = {userId:""};
	$scope.showProfile = false;
	$scope.showAddevent = false;

 

	 $scope.initUser = function(){
		if(getCookie("loggedInUserId")!=""){
			var respData 		= apiServices.fetchUserSettings(getCookie("loggedInUserId"));
			$scope.loggedinUser = respData.status=="00"?respData.header:{userId:""};
		}	
	 }

	 $scope.fetchOrgContents = function(){
		usrId = getCookie("loggedInUserId");
		var respData 		= apiServices.fetchOrgProfile(usrId);
		if(respData.status=="00"){
			$scope.orgContents.orgTitle = respData.data.orgName;
			var orgId = respData.data.orgId;
			var resp = apiServices.fetchOrgEventsByID(orgId);
			$scope.orgContents.conts = resp.data;
			// console.log(JSON.stringify(resp));
			if(resp.status!="00"){
				console.log(JSON.stringify(resp));
			}
		}else{
			console.log(JSON.stringify(respData));
		}		
	 }


	$scope.userImg={"fileVal":""};
	

	

	 $scope.updateOrg 	= function(){
		console.log(JSON.stringify($scope.orgProfile));
		var respData 		= apiServices.updateOrgProfile($scope.orgProfile);
		alert(respData.message);	
	 }

	 $scope.viewProfile = function(){
		var respData = apiServices.fetchOrgProfile(getCookie("loggedInUserId"));
		if(respData.status=="00"){
			$scope.orgProfile = respData.data;
		}	
		$scope.showAddevent = false;
		$scope.showProfile = true;
	 }


	 $scope.saveEvent = function(){
		$scope.eventData.keyUserID = getCookie("loggedInUserId");
		// console.log("Sending event :"+JSON.stringify($scope.eventData.image))
		var resp;
		if($scope.eventData.btn=='Update'){
			resp = apiServices.updateEvent(JSON.stringify($scope.eventData));
		}else{
			resp = apiServices.addEvent(JSON.stringify($scope.eventData));
			if(resp.status=="00"){
				$scope.orgContents.conts.push($scope.eventData);
			}
		}
		alert(resp.message);
	 }

	 $scope.showUpdateEvent = function(eventData){
		$scope.eventData  = eventData;
		$scope.eventData.eventOps = "Update Event";
		$scope.eventData.btn = "Update";
	 }

	 $scope.showAddEvent = function(eventData){
		$scope.eventData.eventOps = "Add Event";
		$scope.eventData.btn = "Save";
	 }

	 $scope.showContents = function(){
		//fetch user events
		var resp = apiServices.fetchOrgEvents(getCookie("loggedInUserId"));
		console.log(JSON.stringify(resp));
		$scope.orgContents.conts = resp.data;
		$scope.showAddevent= false;
		$scope.showProfile = false;
	 }

	 
	 $scope.logout = function(){
		signOut();
	 }


	 var execf1 = $scope.initUser();
	 var execf2 = $scope.fetchOrgContents();
});


function signOut(){
	alert("signing out")
	document.cookie = "loggedInUserId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
	navToIndex();
}
app.directive("fileread", [function () {
		return {
			scope: {
				fileread: "="
			},
			link: function (scope, element, attributes) {
				element.bind("change", function (changeEvent) {
					var reader = new FileReader();
					reader.onload = function (loadEvent) {
						scope.$apply(function () {
							scope.fileread = [];
							scope.fileread2 = loadEvent.target.result;
							scope.fileread.push(scope.fileread2);
							//alert(JSON.stringify(scope.fileread[0]));
						});
					}
					reader.readAsDataURL(changeEvent.target.files[0]);
					
				});
				
			}
		}
	}]);

function navToIndex(){
	window.location.href="index.html";
}
function navToSignIn(){
	window.location.href="signin.html";
}
function navToSignU(){
	window.location.href="signup.html";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function navToLogin(){
	window.location.href="login.html";
	alert("Welcoe Man");
}