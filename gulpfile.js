/**
 * Created by pharaoh on 10/1/2018.
 */

'use strict';
const gulp = require('gulp');
const sass = require("gulp-ruby-sass");
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const livereload = require("gulp-livereload");



gulp.task('style',function () {
    sass('web/assets/scss/**/*.scss',{ style: 'expanded' })
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gulp.dest('web/assets/css/'))
        .pipe(livereload());
});

//minify all js to one js file
gulp.task('minscripts', function() {
    return gulp.src(['web/assets/js/jquery-migrate.js', 'web/assets/js/jquery.waypoints.min.js','web/assets/js/popper.js', 'web/assets/js/bootstrap.min.js','web/assets/js/wow.min.js','web/assets/js/bootstrap-progressbar.min.js', 'web/assets/js/jquery.counterup.min.js','web/assets/js/slick.min.js', 'web/assets/js/jquery.magnific-popup.min.js' ,'web/assets/js/jquery.filterizr.min.js'])
        .pipe(concat('plugins.js'))
        .pipe(gulp.dest('web/assets/'));
});

//minify all css to one css file

gulp.task('mincss',function () {
    return gulp.src(['web/assets/css/bootstrap.min.css','web/assets/css/animate.min.css','web/assets/css/et-line.css','web/assets/css/font-awesome.min.css','web/assets/css/magnific-popup.css','web/assets/css/slick.css','web/assets/css/ionicons.min.css'])
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('web/assets/'))
});


gulp.task('watch',function () {
    livereload.listen();
    gulp.watch('web/assets/scss/**/*.scss',['style'])
});

gulp.task('default',['style','watch']);